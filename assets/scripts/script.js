AOS.init({
    duration: 900,
  })


const backToTop = document.querySelector("#to-top");

function scrollFunction() {
  if(window.pageYOffset > 1500) {
    backToTop.style.display = "block";
  } else {
    backToTop.style.display = "none";

  }
}

window.addEventListener("scroll", scrollFunction);
